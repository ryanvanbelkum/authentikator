# AuthentiKator

This repository contains a Kotlin microservice, "AuthentiKator".  This service will handle user authentication and permissions for any microservice.  

## Specs

    Be patient...

## Running

To run the application, simply invoke the provided JAR like so:

    java -jar <path/to/project>/dist/AuthentiKator-1.0-all.jar

## Building

To build the application, use the provided `gradlew` tool:

    ./gradlew clean shadowJar