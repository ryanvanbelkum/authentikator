package nerdery

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class AuthentiKator

fun main(args: Array<String>) {
    SpringApplication.run(AuthentiKator::class.java, *args)
}