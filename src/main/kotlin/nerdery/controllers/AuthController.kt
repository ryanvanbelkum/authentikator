package nerdery.controllers

import nerdery.models.AuthRequest
import nerdery.models.Session
import nerdery.models.User
import nerdery.services.AuthService
import org.apache.commons.lang3.RandomStringUtils
import org.rs3vans.kt.krypto.hashPassword
import org.rs3vans.kt.krypto.matchesPasswordHash
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import java.util.*

/**
 * Created by rvanbelk on 7/3/16.
 */

@RestController
class AuthController @Autowired constructor(val authService: AuthService) {

//    ROUTES
    @RequestMapping("/login",
            method = arrayOf(RequestMethod.POST),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun signin(@RequestBody user: AuthRequest): ResponseEntity<Any> {
        val user = login(user.username, user.password)
        if (user != null) {
            val session = Session(user.uuid!!, RandomStringUtils.randomAlphanumeric(30).toUpperCase())
            return ResponseEntity<Any>(session, HttpStatus.OK)
        } else {
            return ResponseEntity<Any>("Sorry", HttpStatus.UNAUTHORIZED);
        }
    }

    @RequestMapping("/new",
            method = arrayOf(RequestMethod.POST),
            produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun newUser(@RequestBody user: User) = createNewUser(user)

//    PRIVATE FUNCS
    private fun login(username: String, password: String): User? {
        val user = authService.getStoredUser(username)
        if (user != null && password.matchesPasswordHash(user.password)) {
            return user
        } else {
            return null
        }
    }

    private fun createNewUser(user: User): ResponseEntity<String> {
        if (authService.getStoredUser(user.username) == null) {
            authService.createUser(User(
                    uuid = UUID.randomUUID(),
                    username = user.username,
                    password = user.password.hashPassword(),
                    desciption = "admin",
                    firstname = "ryan",
                    lastname = "vanbelkum"
            ))
            return ResponseEntity<String>("user created", HttpStatus.OK)
        } else {
            return ResponseEntity<String>("user already exists", HttpStatus.CONFLICT)
        }
    }
}