package nerdery.dao

import nerdery.models.User
import nerdery.models.mappers.UserRowMapper
import org.rs3vans.kt.klogger.WithLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataAccessException
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.SQLException
import java.util.*
import javax.sql.DataSource

/**
 * Created by rvanbelk on 7/19/16.
 */
@Repository
open class AuthDao @Autowired constructor(val dataSource: DataSource) {

    val jdbcTemplate: JdbcTemplate = JdbcTemplate(dataSource)

    open fun insertNewUser(user: User) {
        try {
            val statement = "INSERT INTO users (`useruuid`, `username`, `password`, `description`, `firstname`, `lastname`) " +
                    "VALUES (?, ?, ?, ?, ?, ?)"
            trace { "insert new user SQL: $statement" }
            jdbcTemplate.update(statement, user.uuid, user.username, user.password, user.desciption, user.firstname, user.lastname)
        } catch (e: DataAccessException) {
            error("Unable to insert user ${user.username} into DB...", e)
        }
    }

    open fun retrieveUserByUsername(username: String): User? {
        var user: User? = null
        try {
            val statement = "SELECT * FROM users WHERE username = ?"
            trace { "retrieve user by username SQL: $statement" }
            user = jdbcTemplate.queryForObject(
                    statement, arrayOf<Any>(username), UserRowMapper())

        } catch (e: DataAccessException) {
            error("Unable to retrieve user $username from DB...", e)
        }
        return user
    }

    open fun retrieveUserByUuid(uuid: UUID): User? {
        var user: User? = null
        try {
            val statement = "SELECT * FROM users WHERE useruuid = ?"
            trace { "retrieve user by uuid SQL: $statement" }
            user = jdbcTemplate.queryForObject(
                    statement, arrayOf<Any>(uuid), UserRowMapper())

        } catch (e: DataAccessException) {
            error("Unable to retrieve user $uuid from DB...", e)
        }
        return user
    }

    open fun deleteUser(username: String) {
        val statement = "DELETE FROM users WHERE username=?"
        trace { "delete user SQL: $statement" }
        try {
            jdbcTemplate.update(statement, username);
        } catch (e: SQLException) {
            error("Unable to delete user $username from DB", e)
        }
    }

    companion object : WithLogging()
}
