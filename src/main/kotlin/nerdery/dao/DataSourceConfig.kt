package nerdery.dao

import nerdery.dbUrl
import nerdery.password
import nerdery.user
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import javax.sql.DataSource

/**
 * Created by rvanbelk on 7/20/16.
 */
@Configuration
open class DataSourceConfig(){

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    open fun dataSource(): DataSource {
        return DataSourceBuilder
                .create()
                .url(dbUrl)
                .username(user)
                .password(password).build()
    }
}