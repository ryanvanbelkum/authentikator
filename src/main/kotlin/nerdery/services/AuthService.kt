package nerdery.services

import nerdery.dao.AuthDao
import nerdery.models.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Created by rvanbelk on 7/19/16.
 */
@Service
class AuthService @Autowired constructor(val authDao: AuthDao) {

    fun getStoredUser(username: String): User? {
        return authDao.retrieveUserByUsername(username)
    }

    fun saveSession(sessionkey: String) {

    }

    fun createUser(user: User) {
        authDao.insertNewUser(user)
    }
}