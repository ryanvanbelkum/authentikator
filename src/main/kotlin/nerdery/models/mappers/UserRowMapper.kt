package nerdery.models.mappers

import nerdery.models.User
import org.springframework.jdbc.core.RowMapper
import java.sql.ResultSet
import java.util.*

/**
 * Created by rvanbelk on 7/21/16.
 */

open class UserRowMapper: RowMapper<User>
{
    override fun mapRow(rs: ResultSet, rowNum: Int): User
    {
        val user = User(
                UUID.nameUUIDFromBytes(rs.getBytes("useruuid")),
                rs.getString("username"),
                rs.getString("password"),
                rs.getString("description"),
                rs.getString("firstname"),
                rs.getString("lastname"))
        return user;
    }
}