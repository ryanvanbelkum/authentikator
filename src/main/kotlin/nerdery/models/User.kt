package nerdery.models

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

/**
 * Created by rvanbelk on 7/19/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class User @JsonCreator constructor(
        @JsonProperty("useruuid") val uuid: UUID?,
        @JsonProperty("Username") val username: String,
        @JsonProperty("Password") val password: String,
        @JsonProperty("Description") val desciption: String,
        @JsonProperty("Firstname") val firstname: String,
        @JsonProperty("Lastname") val lastname: String) {
}