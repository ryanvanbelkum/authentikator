package nerdery.models

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by rvanbelk on 7/19/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class AuthRequest @JsonCreator constructor(
        @JsonProperty("Username") val username: String,
        @JsonProperty("Password") val password: String) {
}